import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

/**
 * Generated class for the RegisterIntroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-register-intro',
    templateUrl: 'register-intro.html',
})
export class RegisterIntroPage {
    
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }
    
    ionViewDidLoad() {
        console.log('ionViewDidLoad RegisterIntroPage');
    }
    
    goToRegister(type)
    {
        this.navCtrl.push('RegisterPage', {type:type});
    }

    goToLogin()
    {
        this.navCtrl.push('LoginPage');
    }
}

